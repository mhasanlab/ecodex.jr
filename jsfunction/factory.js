// Dynamic Object With Factory Pattern

function addNewStudent(id, name, course, price){
    var std = new Object();
    
    std.id = id;
    std.name = name;
    std.course = course;
    std.price = price;
    std.tax = salesTax;
    std.details = function(){
        return `
        ID: ${this.id}
        Name: ${this.name}
        Course: ${this.course}
        Price: ${this.price} `;
    }
    return std;
}

function salesTax(){
    var totalTax = Number(this.price) * Number(.10)
    return totalTax
}

var student = addNewStudent(1, 'Abul Kalam', 'Web Development', 25000);

console.log('\t\tFactory Pattern Output:\n')

console.log(`${student.details()}
\t\tTotal Tax: ${student.tax()}`);

console.log('       ======================\n');