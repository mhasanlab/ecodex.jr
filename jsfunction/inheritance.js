// Prototypal Inheritance in JavaScript

function Employee (id, name, salary){

    this.id = id;
    this.name = name;
    this.salary = salary;
}

Employee.prototype.position = function(){
    
    return ("Software Developer")
}	

function Programmer (name, id, salary, language){
    
    Employee.call(this, name, id, salary);
    this.language = language;
}
    
Programmer.prototype = Object.create(Employee.prototype);
Programmer.prototype.constructor = Programmer;


    var emp = new Programmer (1, "Abul Kalam", 25000, "C# .NET");

console.log(`ID: ${emp.id}\nName: ${emp.name}\nPosition: ${emp.position()}\nLanguage: ${emp.language}\nSalary: ${emp.salary}`);
