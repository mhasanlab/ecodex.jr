// Literal Pattern Object

var Employee = {
    id: 1,
    name:'Abul Kalam',
    department: 'Web Development',
    position: 'Jr. Programmer',
    language: 'JavaScript',
    salary: 20000,
    details: function(){
        return `
        ID: ${this.id}
        Name: ${this.name}
        Department: ${this.department}
        Position: ${this.position}
        Language: ${this.language}
        Salary: ${this.salary} `;
    }
}

console.log('\t\tLiteral Pattern Output:\n')
console.log(Employee.details());
console.log('       ======================\n');
