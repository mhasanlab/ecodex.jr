// Literal Pattern Object

var Employee = {
    id: 1,
    name:'Abul Kalam',
    department: 'Web Development',
    position: 'Jr. Programmer',
    language: 'JavaScript',
    salary: 20000,
    details: function(){
        return `
        ID: ${this.id}
        Name: ${this.name}
        Department: ${this.department}
        Position: ${this.position}
        Language: ${this.language}
        Salary: ${this.salary} `;
    }
}

console.log('\t\tLiteral Pattern Output:\n')
console.log(Employee.details());
console.log('       ======================\n');



// Dynamic Object With Factory Pattern

function addNewStudent(id, name, course, price){
    var std = new Object();
    
    std.id = id;
    std.name = name;
    std.course = course;
    std.price = price;
    std.tax = salesTax;
    std.details = function(){
        return `
        ID: ${this.id}
        Name: ${this.name}
        Course: ${this.course}
        Price: ${this.price} `;
    }
    return std;
}

function salesTax(){
    var totalTax = Number(this.price) * Number(.10)
    return totalTax
}

var student = addNewStudent(1, 'Abul Kalam', 'Web Development', 25000);

console.log('\t\tFactory Pattern Output:\n')

console.log(`${student.details()}
\t\tTotal Tax: ${student.tax()}`);

console.log('       ======================\n');



// Constructor Pattern with prototype object

function Student (id, name, course, price){
    this.id =id;
    this.name = name;
    this.course = course;
    this.price = price;
    this.details = function(){
        return `
        ID: ${this.id}
        Name: ${this.name}
        Course: ${this.course}
        Price: ${this.price} `;
    }
}

Student.prototype.salesTax = function(tax){
    return (this.price) * (tax)
}

var student = new Student(1, 'Abul Kalam', 'Software Development', 50000);

console.log('\t\tPrototype Method Output:\n')

console.log(`${student.details()}
\t\tSales Tax: ${student.salesTax(.10)} `)

console.log('       ======================\n');



	// Namespace in JavaScript

	var employee = {
        company: function () {
            console.log ("Mainul is a programmer working in ");
            }
        }
    
        var employer = {
        company: function () {
        console.log ("ABC Software Company");
        }
        }
        
        employee.company();
        employer.company();

        

// Prototypal Inheritance in JavaScript

function Employee (id, name, salary){

    this.id = id;
    this.name = name;
    this.salary = salary;
}

Employee.prototype.position = function(){
    
    return ("Software Developer")
}	

function Programmer (name, id, salary, language){
    
    Employee.call(this, name, id, salary);
    this.language = language;
}
    
Programmer.prototype = Object.create(Employee.prototype);
Programmer.prototype.constructor = Programmer;


    var emp = new Programmer (1, "Abul Kalam", 25000, "C# .NET");

console.log(`ID: ${emp.id}\nName: ${emp.name}\nPosition: ${emp.position()}\nLanguage: ${emp.language}\nSalary: ${emp.salary}`);
