# what is this?

Collection of JavaScript function , object & class. This is a set of JavaScript Function, object, namespace & class this will give you an idea of JavaScript function. Also you will get a perfect example of JavaScript Function and objects.

# Installation
`npm init`

```
{
  "name": "projectname",
  "version": "1.0.0",
  "description": "My First Project",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Author Name",
  "license": "ISC",
  "dependencies": {
    "ecodex.jr": "^1.1.0"
  }
}

```

Then...

`npm install ecodex.jr`


# Topics

* literal Pattern Object in JavaScript

* Dynamic Object with Factory Pattern in JavaScript

* Constructor Pattern with prototype method in JavaScript

* Prototypal Inheritance in JavaScript

* Namespace in JavaScript

* Class in JavaScript

* JavaScript Form Validation with NodeJs & Jquery

* Asynchronous operation in JavaScript

