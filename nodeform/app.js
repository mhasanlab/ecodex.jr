
var express = require('express');
var formidable = require('formidable');
var app = express();

app.use(express.static(__dirname + '/www'));
app.get('/submitForm', function (req, res) {
    res.send({
        name: req.query.fname + " " + req.query.lname,
        email: req.query.email,
        phone: req.query.phone
    });
});

app.post('/submitForm', function (req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fld) {
        res.send({
            name: fld.fname + " " + fld.lname,
            email: fld.email,
            phone:fld.phone
        })
    })
});

app.listen(8080);
console.log("Server is running at 8080 port");